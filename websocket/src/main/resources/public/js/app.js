var wsocket = new SockJS('/healthz');
var client = Stomp.over(wsocket);

function showMessage(message) {
    $("#message").append(message + "<br/>");
}

client.connect({}, function(frame) {
	$("#message").append("Connection established...<br/><br/>");
	console.log("connected...");
    client.subscribe('/monitoring/notify', function (message) {
    	console.log(message.body);
        showMessage(message.body);
    });
});
