package personal.websocket.model;

public enum ACTION {
	ADD,
	UPDATE,
	DELETE,
	GET
}
