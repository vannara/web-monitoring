package personal.websocket.model;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Item {
	private int itemId;
	private String description;
	private ACTION action;
	
	public Item(int id, String des, ACTION action) {
		this.itemId = id;
		this.description = des;
		this.action = action;
	}
	
    public ACTION getAction() {
		return action;
	}

	public void setAction(ACTION action) {
		this.action = action;
	}


    public String getDescription() {
        return this.description;
    }

    public void setdescription(String des) {
        this.description = des;
    }
    
    public int getId() {
        return this.itemId;
    }

    public void setId(int id) {
        this.itemId = id;
    }
    
    public String getItemMessage() {
    	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); //2019/03/25 12:08:43
    	Date date = new Date();
    	
    	String message = dateFormat.format(date) 
    			+ " ==> The row with id " 
    			+ this.itemId 
    			+ (this.action == ACTION.ADD ? 
    					" was inserted. " : this.action == ACTION.UPDATE ? 
    					" was updated." : this.action == ACTION.DELETE ? 
    					" was deleted." : " is returned");
    	System.out.println(message); 
    	
    	return message;
    }
}
