package personal.websocket.service;

import personal.websocket.model.*;

public interface ItemRepository {
	Item GetItem(int id);
	Item AddNew(String description);
	Item Update(int id, String description);
	Item Delete(int id);
}
