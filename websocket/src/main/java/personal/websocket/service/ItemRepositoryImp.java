package personal.websocket.service;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import personal.websocket.model.ACTION;
import personal.websocket.model.Item;

@Component
public class ItemRepositoryImp implements ItemRepository {
	private ArrayList<Item> items;
	
	public ItemRepositoryImp() {
		items = new ArrayList<Item>();
	}
	@Override
	public Item AddNew(String description) {
		int id = items.size() + 1;
		Item item = new Item (id, description,ACTION.ADD);
		items.add(item);
		return item;
	}

	@Override
	public Item Update(int id, String description) {
		if (items.size()>0 && id >=1) {
			Item item = items.get(id - 1);
			if (item != null) {
				item.setdescription(description);
				item.setAction(ACTION.UPDATE);
				return item;
			}
		}
		return null;
	}

	@Override
	public Item Delete(int id) {
		if (items.size()>0 && id >=1) {
			Item item = items.get(id - 1);
			if (item != null) {
				item.setAction(ACTION.DELETE);
				return item;
			}
		}
		return null;
	}
	@Override
	public Item GetItem(int id) {
		if (items.size()>0 && id >=1) {
			Item item = items.get(id - 1);
			if (item != null) {			
				return item;
			}
		}
		return new Item(0, "fake", ACTION.GET);
	}

	
	
}
