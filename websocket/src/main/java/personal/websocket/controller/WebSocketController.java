package personal.websocket.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

import personal.websocket.model.Item;
import personal.websocket.model.Notification;

@Controller
public class WebSocketController {

    @MessageMapping("/websocket")
    @SendTo("/monitoring/notify")
    public Notification notify(String message) throws Exception {
        Thread.sleep(1000); // simulated delay
        return new Notification(message);
    }

}
