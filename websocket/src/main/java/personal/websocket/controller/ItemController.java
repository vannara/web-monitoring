package personal.websocket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import personal.websocket.model.Item;
import personal.websocket.service.ItemRepository;

@RestController
@RequestMapping("/")
public class ItemController {
	private final ItemRepository repository;
	private Item item;
	@Autowired
	private SimpMessagingTemplate template;
	
	ItemController(ItemRepository repository) {
		this.repository = repository;
	}
	@GetMapping(value="/item/{id}", headers="Accept=application/json")
	public void GetItem(@PathVariable int id) throws Exception {
		item = repository.GetItem(id);
		// notify action to websocket controller
		 this.template.convertAndSend("/monitoring/notify", item.getItemMessage());
	}
	@PostMapping(value="/item/{description}",headers="Accept=application/json")
	public void AddItem(@PathVariable String description) throws Exception {
		item = repository.AddNew(description);
		// notify action to websocket controller
		this.template.convertAndSend("/monitoring/notify", item.getItemMessage());
	}
	
	@PutMapping(value="/item/{id}/{description}", headers="Accept=application/json")
	public void UpdateItem(@PathVariable int id, @PathVariable String description) throws Exception {
		item = repository.Update(id, description);
		// notify action to websocket controller
		this.template.convertAndSend("/monitoring/notify", item.getItemMessage());
	}
	
	@DeleteMapping(value="/item/{id}", headers="Accept=application/json")
	public void DeleteItem(@PathVariable int id) throws Exception  {
		item = repository.Delete(id);
		// notify action to websocket controller
		this.template.convertAndSend("/monitoring/notify", item.getItemMessage());
	}
}
