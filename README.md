# Web client monitoring

Using websocket for the communication between client and server

## Pre-requisites
* JDK 1.8
* Java 8
* Maven 3.2+

## application.properties
* server.port = 8081

## Installation
* cd websocket then do mvn install

## Run
* mvn spring-boot:run

## Launch
* http://localhost:8081

## Calling REST services
### example
* curl -X POST http://localhost:8081/item/description -> add new item
* curl -X PUT http://localhost:8081/item/2/newdescription -> update item with id and new description
* curl -X DELETE http://localhost:8081/item/2/ -> delete item with id
* curl -X GET http://localhost:8081/item/2/ -> get item with id




